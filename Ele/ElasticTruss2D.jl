type ElasticTruss2D <: Line2D

    # ---二维桁架--- #
    tag::Int64 #// 标签
    NodeI::Node #// 节点I
    NodeJ::Node #// 节点J
    Sec::Section #// 单元截面

    len::Float64 #// 单元长度
    
    nDof::Int64 

    vecx::Vector{Float64} #// 局部x轴
    vecy::Vector{Float64} #// 局部y轴

    Ke::Matrix{Float64} #// 单元关键刚度矩阵, 1X1
    # Kl::Matrix{Float64} #// 局部刚度矩阵, 4X4
    Kg::Matrix{Float64} #// 整体刚度矩阵, 4X4
    # Ml::Matrix{Float64} #// 局部质量矩阵, 4X4
    Mg::Matrix{Float64} #// 整体质量矩阵, 4X4

    Te2l::Matrix{Float64} #// 坐标转换矩阵, 1X4
    Tl2g::Matrix{Float64} #// 坐标转换矩阵, 4X4
    Te2g::Matrix{Float64} #// 坐标转换矩阵, 1X4

    dofVector::Vector{Int64}    # 自由度编号矩阵
    dofMatrix::Matrix{Int64}    # 自由度编号矩阵

    deform::Float64 #// 单元轴向变形
    force::Float64 #// 单元轴向力
    # strain::Float64 #// 单元轴向应变
    # stress::Float64 #// 单元轴向应力
    nodeForces::Vector{Float64} #// 

    function ElasticTruss2D(tag::Int64, Nodes::Vector{Node}, Sec::Section)

        NodeI, NodeJ = Nodes
        nDof = 4
        nDofl = 2

        vecx = NodeJ.coord .- NodeI.coord
        len = norm(vecx)
        vecx = vecx/len

        # len,vecx,vecy = GetLine2DGeo(NodeI,NodeJ)
        lsys = LocalSystem(2,vecx)
        vecy = lsys.J

        a_x, b_x = vecx
        a_y, b_y = vecy

        Te2l = [-1. 0. 1. 0.]
        Tl2g = [
        [a_x a_y 0.0 0.0],
        [b_x b_y 0.0 0.0],
        [0.0 0.0 a_x a_y],
        [0.0 0.0 b_x b_y],
        ]
        Te2g = [-a_x -a_y a_x a_y]

        Ke = ones(1,1)
        # Kl = ones(nDof,nDof)
        Kg = ones(nDof,nDof)
        # Ml = ones(nDof,nDof)
        Mg = ones(nDof,nDof)

        deform = 0.0
        force = 0.0
        nodeForces = zeros(nDof)

        dofVector = zeros(Int64,nDof) # 矩阵组装
        dofMatrix = zeros(Int64,nDof,nDof) # 矩阵组装

        return new(tag, NodeI, NodeJ, 
                    deepcopy(Sec), len, nDof,
                    vecx, vecy, 
                    # Ke, Kl, Kg, Ml, Mg, 
                    Ke, Kg, Mg, 
                    Te2l, Tl2g, Te2g, 
                    dofVector ,dofMatrix,
                    deform, force,
                    nodeForces,
                    )
    end
end

function getDofMatrix(line::ElasticTruss2D)

    dofOrders = [line.NodeI.dofOrders,line.NodeJ.dofOrders]
    line.dofVector = dofOrders
    line.dofMatrix = [dofOrders dofOrders dofOrders dofOrders]

    nothing

end

function setDeform(line::ElasticTruss2D)
    
    disp = [line.NodeI.disp,line.NodeJ.disp]
    deform = line.Te2g*disp
    line.deform = deform[1]
    nothing
    
end

function getStiff(line::ElasticTruss2D)
    line.Ke[1,1] = line.Sec.Da/line.len
    nothing
end

function getStiffMatrix(line::ElasticTruss2D)
    line.Kg = (line.Te2g).'*line.Ke*line.Te2g
    nothing
end

function getMassMatrix(line::ElasticTruss2D,lumped::Bool=true)
    
    lineMass = line.Sec.rho*line.len
    if lumped
        Mg = lineMass/2.0.*[
                            [1.0 0.0 0.0 0.0],
                            [0.0 1.0 0.0 0.0],
                            [0.0 0.0 1.0 0.0],
                            [0.0 0.0 0.0 1.0],
                            ]
    else
        Mg = lineMass/6.0.*[
                            [2.0 0.0 1.0 0.0],
                            [0.0 2.0 0.0 1.0],
                            [1.0 0.0 2.0 0.0],
                            [0.0 1.0 0.0 2.0],
                                    ]
    end
    Mg[1,1] += line.NodeI.mass[1]
    Mg[2,2] += line.NodeI.mass[2]
    Mg[3,3] += line.NodeJ.mass[1]
    Mg[4,4] += line.NodeJ.mass[2]

    line.Mg = Mg
    nothing
end

function getForce(line::ElasticTruss2D)
    setStrain(line.Sec,line.deform/line.len)
    getForce(line.Sec)
    line.force = line.Sec.force
    line.nodeForces = line.force*line.Te2g.'
    nothing
end

function getForceStiff(line::ElasticTruss2D)
    setStrain(line.Sec,line.deform/line.len)
    getForceStiff(line.Sec)
    line.force = line.Sec.force
    line.nodeForces = line.force*line.Te2g.'
    line.Ke[1,1] = line.Sec.Da/line.len
    nothing
end