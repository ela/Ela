module Geo

    export Node, RefPoint,
            setDofOrder, setDisp, getUnbalancedForce, print

    type Node

        # ---节点类--- #
        tag::Int64 #// 标签
        coord::Vector{Float64} #// 坐标
        dim::Int64 #// 维度数
        dof::Int64 #// 自由度数
        mass::Vector{Float64}  #// 节点集中质量向量, length = dof
        dofOrders::Vector{Int64} #// 整体模型中节点的自由度编号
        disp::Vector{Float64}  #// 节点位移向量, length = dof
        vel::Vector{Float64}  #// 节点速度向量, length = dof
        acc::Vector{Float64}  #// 节点加速度向量, length = dof
        react::Vector{Float64}  #// 节点反力向量(仅对受约束自由度有意义), length = dof

        function Node(tag::Int64,coord::Vector{Float64})
            dim = length(coord)
            
            if dim == 2
                dof = 3
            else
                dof = 6
            end

            mass      = zeros(dof)
            dofOrders = zeros(Int64,dof)
            disp      = zeros(dof)
            vel       = zeros(dof)
            acc       = zeros(dof)
            react  = zeros(dof)

            return new(tag,coord,dim,dof,mass,dofOrders,disp,vel,acc,react)
        end

        function Node(tag::Int64,coord::Vector{Float64},mass::Vector{Float64},dim::Int64,dof::Int64)

            coord = coord[1:dim]
            mass  = mass[1:dof]
            
            dofOrders = zeros(Int64,dof)
            disp      = zeros(dof)
            vel       = zeros(dof)
            acc       = zeros(dof)
            react  = zeros(dof)

            return new(tag,coord,dim,dof,mass,dofOrders,disp,vel,acc,react)
        end
    end

    function setDofOrder(node::Node,orders::Vector{Int64})
        # -- 设置节点自由度在整体模型中的标签 -- #
        node.dofOrders = orders
        nothing
    end

    function setDofOrder(node::Node,id::Int64,order::Int64)
        # -- 设置节点自由度在整体模型中的标签 -- #
        node.dofOrders[id] = order
        nothing
    end

    function setDisp(node::Node,id::Int64,disp::Float64)
        # -- 设置节点自由度在整体模型中的标签 -- #
        node.disp[id] = disp
        nothing
    end

    function setDisp(node::Node,U::Vector{Float64})
        for i in 1:node.dof
            node.disp[i] = U[node.dofOrders[i]]
        end
        nothing
    end

    function print(node::Node,withDetails::Bool=false)

        println("Node $(node.tag)")
        println("Coord: $(node.coord)")

        if withDetails
            println("Lumped Mass: $(node.mass)")
            println("Dof Orders in SysEquation: $(node.dofOrders)")
            println("Displacement: $(node.disp)")
            println("Unbalanced Force: $(node.ubforce)")
        end

    end

    function setReaction(node::Node,id::Int64,rf::Float64)
        node.react[id] = rf
        nothing
    end

    type RefPoint

        # ---参考点类--- #
        tag::Int64 #// 标签
        coord::Vector{Float64} #// 坐标
        
    end

end
