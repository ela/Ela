module Bc

# ---- 边界条件 ---- #

    importall Geo

    export Boundary, ConstantBoundary, CNodeForce, CNodeDisp, getValue

    abstract Boundary
    abstract ConstantBoundary <: Boundary
    abstract ProportionBoundary <: Boundary
    abstract TimeHistoryBoundary <: Boundary

    # abstract CNodeForce <: ConstantBoundary
    # abstract CNodeDisp <: ConstantBoundary

    type CNodeForce <: ConstantBoundary

        node::Node #// 荷载节点
        dofTag::Int64 #// 荷载局部自由度
        dofValue::Float64 #// 荷载自由度

        function CNodeForce(node::Node, dofTag::Int64, dofValue::Float64)
            dofOrder = 1
            return new(node, dofTag, dofValue)
        end
    end

    type CNodeDisp <: ConstantBoundary

        node::Node #// 约束节点
        dofTag::Int64 #// 约束自由度
        dofValue::Float64 #// 约束值

        function CNodeDisp(node::Node, dofTag::Int64, dofValue::Float64)
            dofOrder = 1
            return new(node, dofTag, dofValue)
        end

    end

    function getValue(bc::ConstantBoundary,time::Float64)
        return bc.dofValue,0.0 #// dofValue,delta_dofValue
    end

    function getValue(bc::ConstantBoundary,step::Int64)
        return bc.dofValue,0.0 #// dofValue,delta_dofValue
    end

    function setDofIndex(bc::ConstantBoundary,indx::Int64)
        # -- 设置节点自由度在整体模型中的标签 -- #
        bc.dofOrder = indx
        nothing
    end

end