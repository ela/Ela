push!(LOAD_PATH, "..")
using Ela,Cal
import Mat,Sec,Ele

function Truss2d()
	
	dim, dof = 2,2
	truss2d = Domain(dim, dof, "truss2d")

	matType = Mat.Elastic1D
	matTag = 1
	rho = 7850.
	E0 = 2.06e11

	addMat(matType,matTag,rho,E0)

	secType = Sec.ElasticTrussSection
	secTag = 1
	A = 0.00282743338823082
	mat = truss2d.Mats[matTag]
	addSec(secType,secTag,A,mat)

	l = 3.0
	h = 3.0
	nl = 6

	for nodeTag = 1:nl+1
		coord = [l*(nodeTag-1),0.0]
		addNode(nodeTag,coord)
	end

	for nodeTag = nl+2:2*nl+1
		coord = [l*0.5+l*(nodeTag-nl-2),h]
		addNode(nodeTag,coord)
	end

	addFix(1,1)
	addFix(1,2)
	addFix(7,1)
	addFix(7,2)

	eleType = Ele.ElasticTruss2D
	sec = truss2d.Secs[secTag]
	for eleTag = 1:nl
		addEle(eleType,eleTag,[eleTag,eleTag+1],sec)
		addEle(eleType,eleTag+2*nl-1,[eleTag,eleTag+7],sec)
		addEle(eleType,eleTag+3*nl-1,[eleTag+1,eleTag+7],sec)
	end
	for eleTag = nl+1:2*nl-1
		addEle(eleType,eleTag,[eleTag+1,eleTag+2],sec)
	end

	buildModel()
	saveModel()

	mode = EigenAnalysis(truss2d,12)
	execAnalysis(mode)
	# saveResult(mode)

	return truss2d

end

runtime0 = @elapsed truss2d = Truss2d();
runtime1 = @elapsed truss2d = Truss2d();
# println(sort(collect(keys(truss2d.Nodes))))
# println(sort(collect(keys(truss2d.Eles))))
# println(collect(keys(truss2d.Cons)))
# println(truss2d.linalgsys.K)
# println(truss2d.linalgsys.M)
println(runtime0)
println(runtime1)
