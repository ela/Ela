<TeXmacs|1.99.2>

<style|<tuple|article|chinese|number-long-article|framed-session|maxima>>

<\body>
  <doc-data|<doc-title|NSA \<#7406\>\<#8BBA\>>|<doc-author|<author-data|<author-name|Chao>>>>

  <\the-glossary|gly>
    <glossary-2|<with|mode|math|\<Pi\><rsub|p>>|\<#7CFB\>\<#7EDF\>\<#52BF\>\<#80FD\>|<pageref|auto-3>>

    <glossary-2|<with|mode|math|\<b-sigma\>>|\<#5E94\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-4>>

    <glossary-2|<with|mode|math|\<b-varepsilon\>>|\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\>|<pageref|auto-5>>

    <glossary-2|<with|mode|math|\<b-d\>>|\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-6>>

    <glossary-2|<with|mode|math|\<b-F\><rsub|\<b-b\>>>|\<#4F53\>\<#79EF\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-7>>

    <glossary-2|<with|mode|math|\<b-F\><rsub|\<b-s\>>>|\<#4F53\>\<#79EF\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-8>>

    <glossary-2|<with|mode|math|\<b-P\>>|\<#8282\>\<#70B9\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-9>>

    <glossary-2|<with|mode|math|\<b-up-K\><rsub|\<b-e\>>>|\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>\<#FF08\>\<#5BF9\>\<#5E94\>\<#76F8\>\<#5BF9\>\<#53D8\>\<#5F62\>\<#FF09\>|<pageref|auto-10>>

    <glossary-2|<with|mode|math|V>|\<#5355\>\<#5143\>\<#4F53\>\<#79EF\>|<pageref|auto-11>>

    <glossary-2|<with|mode|math|\<b-up-B\><rsub|\<b-e\>>>|\<#5E94\>\<#53D8\>-\<#53D8\>\<#5F62\>\<#5173\>\<#7CFB\>\<#77E9\>\<#9635\>|<pageref|auto-12>>

    <glossary-2|<with|mode|math|\<b-up-D\>>|\<#6750\>\<#6599\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\>|<pageref|auto-13>>

    <glossary-2|<with|mode|math|L>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#957F\>\<#5EA6\>|<pageref|auto-14>>

    <glossary-2|<with|mode|math|\<b-up-D\><rsub|\<b-e\>>>|\<#622A\>\<#9762\>\<#5C42\>\<#6B21\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\>|<pageref|auto-15>>

    <glossary-2|<with|mode|math|\<b-up-T\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#5230\>\<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#7684\>\<#8F6C\>\<#6362\>\<#77E9\>\<#9635\>|<pageref|auto-16>>

    <glossary-2|<with|mode|math|\<b-up-K\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>|<pageref|auto-17>>

    <glossary-2|<with|mode|math|\<b-up-T\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#5230\>\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7684\>\<#8F6C\>\<#6362\>\<#77E9\>\<#9635\>|<pageref|auto-18>>

    <glossary-2|<with|mode|math|\<b-up-K\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>|<pageref|auto-19>>

    <glossary-2|<with|mode|math|\<b-i\>>|\<#6574\>\<#4F53\><with|mode|math|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-21>>

    <glossary-2|<with|mode|math|\<b-j\>>|\<#6574\>\<#4F53\><with|mode|math|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-22>>

    <glossary-2|<with|mode|math|\<b-k\>>|\<#6574\>\<#4F53\><with|mode|math|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-23>>

    <glossary-2|<with|mode|math|\<b-i\><rprime|'>>|\<#5C40\>\<#90E8\><with|mode|math|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-24>>

    <glossary-2|<with|mode|math|\<b-j\><rprime|'>>|\<#5C40\>\<#90E8\><with|mode|math|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-25>>

    <glossary-2|<with|mode|math|\<b-k\><rprime|'>>|\<#5C40\>\<#90E8\><with|mode|math|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-26>>

    <glossary-2|<with|mode|math|\<b-l\>>|\<#6746\>\<#7684\>\<#8F74\>\<#5411\>\<#5411\>\<#91CF\>\<#FF08\>\<#5C40\>\<#90E8\><with|mode|math|x>\<#8F74\>\<#65B9\>\<#5411\>\<#FF09\>|<pageref|auto-28>>

    <glossary-2|<with|mode|math|\<varepsilon\>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#622A\>\<#9762\>\<#8F74\>\<#5411\>\<#5E94\>\<#53D8\>|<pageref|auto-35>>

    <glossary-2|<with|mode|math|u<rsub|e>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\>|<pageref|auto-36>>

    <glossary-2|<with|mode|math|\<b-varepsilon\>>|\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\>|<pageref|auto-37>>

    <glossary-2|<with|mode|math|\<b-u\><rsub|\<b-e\>>>|\<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-38>>

    <glossary-2|<with|mode|math|\<sigma\>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#622A\>\<#9762\>\<#8F74\>\<#5411\>\<#5E94\>\<#529B\>|<pageref|auto-39>>

    <glossary-2|<with|mode|math|D>|\<#6750\>\<#6599\>\<#5207\>\<#7EBF\>\<#6A21\>\<#91CF\>|<pageref|auto-40>>

    <glossary-2|<with|mode|math|A>|\<#622A\>\<#9762\>\<#9762\>\<#79EF\>|<pageref|auto-41>>

    <glossary-2|<with|mode|math|d<around*|(|x|)>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#5185\>\<#90E8\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\>|<pageref|auto-42>>

    <glossary-2|<with|mode|math|\<b-u\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-43>>

    <glossary-2|<with|mode|math|\<b-u\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-44>>
  </the-glossary>

  <section|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>>

  \<#516C\>\<#5F0F\>\<#63A8\>\<#5BFC\>\<#8BF4\>\<#660E\>

  \<#7531\>\<#4E8E\>\<#521A\>\<#5EA6\>\<#4E0E\>\<#521A\>\<#4F53\>\<#4F4D\>\<#79FB\>\<#65E0\>\<#5173\>\<#FF0C\>\<#6545\>\<#63A8\>\<#5BFC\>\<#521A\>\<#5EA6\>\<#53C2\>\<#6570\>\<#65F6\>\<#53EF\>\<#9009\>\<#5355\>\<#5143\>\<#76F8\>\<#5BF9\>\<#53D8\>\<#5F62\>\<#4E3A\>\<#53D8\>\<#5F62\>\<#81EA\>\<#7531\>\<#5EA6\>\<#4EE5\>\<#7B80\>\<#5316\>\<#63A8\>\<#5BFC\>\<#FF0C\>\<#4EA6\>\<#53EF\>\<#5C06\>\<#6750\>\<#6599\>\<#975E\>\<#7EBF\>\<#6027\>\<#76F8\>\<#5173\>\<#7684\>\<#53C2\>\<#6570\>\<#FF08\>\<#9700\>\<#8981\>\<#901A\>\<#8FC7\>\<#6570\>\<#503C\>\<#79EF\>\<#5206\>\<#786E\>\<#5B9A\>\<#FF09\>\<#4E0E\>\<#5176\>\<#5B83\>\<#53C2\>\<#6570\>\<#663E\>\<#5F0F\>\<#5730\>\<#5206\>\<#5F00\>\<#3002\>

  \<#901A\>\<#7528\>\<#516C\>\<#5F0F\>

  \<#7CFB\>\<#7EDF\>\<#52BF\>\<#80FD\>\<#8868\>\<#8FBE\>\<#5F0F\><glossary-explain|<math|\<Pi\><rsub|p>>|\<#7CFB\>\<#7EDF\>\<#52BF\>\<#80FD\>>
  <glossary-explain|<math|\<b-sigma\>>|\<#5E94\>\<#529B\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-varepsilon\>>|\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-d\>>|\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-F\><rsub|\<b-b\>>>|\<#4F53\>\<#79EF\>\<#529B\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-F\><rsub|\<b-s\>>>|\<#4F53\>\<#79EF\>\<#529B\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-P\>>|\<#8282\>\<#70B9\>\<#529B\>\<#5411\>\<#91CF\>>

  <\equation>
    <label|pe>\<Pi\><rsub|p>=<frac|1|2><big|int><rsub|V>\<b-sigma\><rsup|T>\<b-varepsilon\>\<mathd\>V-<big|int><rsup|L><rsub|0>\<b-d\><rsup|T>\<b-F\><rsub|\<b-b\>>\<mathd\>V-<big|int><rsup|L><rsub|0>\<b-d\><rsup|T>\<b-F\><rsub|\<b-s\>>\<mathd\>S-\<b-d\><rsup|T>\<b-P\>
  </equation>

  \;

  \<#5C06\>\<#7CFB\>\<#7EDF\>\<#79BB\>\<#6563\>\<#4E3A\>\<#82E5\>\<#5E72\>\<#5355\>\<#5143\>

  <\equation>
    \;
  </equation>

  \<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#7531\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#63D2\>\<#503C\>\<#5F97\>\<#5230\>

  <\equation>
    <label|u2d>\<b-d\>=\<b-up-N\>\<b-u\>
  </equation>

  \;

  \<#5E94\>\<#53D8\>\<#4E0E\>\<#53D8\>\<#5F62\>\<#7684\>\<#5173\>\<#7CFB\>

  <\equation>
    <label|u2e>\<b-varepsilon\>=<around*|[|\<partial\>|]>\<b-d\>=<around*|[|\<partial\>|]>\<b-up-N\>\<b-u\>=\<b-up-B\>\<b-u\>
  </equation>

  \;

  \<#5E94\>\<#529B\>\<#4E0E\>\<#5E94\>\<#53D8\>\<#7684\>\<#5173\>\<#7CFB\>

  <\equation>
    \<b-sigma\>=\<b-up-D\>\<b-varepsilon\>
  </equation>

  \;

  \<#5C06\>\<#5F0F\><eqref|u2e>\<#4EE3\>\<#5165\>\<#5F0F\><eqref|pe>

  <\equation>
    <label|peu>\<Pi\><rsub|p>=<frac|1|2>\<b-u\><rsup|T>\<cdot\><big|int><rsub|V>\<b-up-B\><rsup|T>\<b-up-D\><rsup|T>\<b-up-B\>\<mathd\>V\<cdot\>\<b-u\>-\<b-u\><rsup|T>\<cdot\><big|int><rsup|L><rsub|0>\<b-up-N\><rsup|T>\<b-F\><rsub|\<b-b\>>\<mathd\>V-\<b-u\><rsup|T>\<cdot\><big|int><rsup|L><rsub|0>\<b-up-N\><rsup|T>\<b-F\><rsub|\<b-s\>>\<mathd\>S-\<b-u\><rsup|T>\<b-P\>
  </equation>

  \;

  \<#6700\>\<#5C0F\>\<#52BF\>\<#80FD\>\<#539F\>\<#7406\>

  <\equation>
    <label|minp><frac|\<mathd\>\<Pi\><rsub|p>|\<mathd\>\<b-u\>>=0
  </equation>

  \;

  \<#7531\>\<#5F0F\><eqref|peu>\<#3001\><eqref|minp>\<#53EF\>\<#5F97\>

  <\equation>
    <big|int><rsub|V>\<b-up-B\><rsup|T>\<b-up-D\><rsup|T>\<b-up-B\>\<mathd\>V\<cdot\>\<b-u\>-<big|int><rsup|L><rsub|0>\<b-up-N\><rsup|T>\<b-F\><rsub|\<b-b\>>\<mathd\>V-<big|int><rsup|L><rsub|0>\<b-up-N\><rsup|T>\<b-F\><rsub|\<b-s\>>\<mathd\>S-\<b-P\>=0
  </equation>

  \;

  \<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>\<#FF08\>\<#5BF9\>\<#5E94\>\<#76F8\>\<#5BF9\>\<#53D8\>\<#5F62\>\<#FF09\><glossary-explain|<math|\<b-up-K\><rsub|\<b-e\>>>|\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>\<#FF08\>\<#5BF9\>\<#5E94\>\<#76F8\>\<#5BF9\>\<#53D8\>\<#5F62\>\<#FF09\>>
  <glossary-explain|<math|V>|\<#5355\>\<#5143\>\<#4F53\>\<#79EF\>>
  <glossary-explain|<math|\<b-up-B\><rsub|\<b-e\>>>|\<#5E94\>\<#53D8\>-\<#53D8\>\<#5F62\>\<#5173\>\<#7CFB\>\<#77E9\>\<#9635\>>
  <glossary-explain|<math|\<b-up-D\>>|\<#6750\>\<#6599\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\>>
  <glossary-explain|<math|L>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#957F\>\<#5EA6\>>
  <glossary-explain|<math|\<b-up-D\><rsub|\<b-e\>>>|\<#622A\>\<#9762\>\<#5C42\>\<#6B21\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\>>

  <\equation>
    \<b-up-K\><rsub|\<b-e\>>=<big|int><rsub|V>\<b-up-B\><rsup|T><rsub|\<b-e\>>\<b-up-D\>\<b-up-B\><rsub|\<b-e\>>\<mathd\>V=<big|int><rsup|L><rsub|0>\<b-up-B\><rsup|T><rsub|\<b-e\>>\<b-up-D\><rsub|\<b-e\>>\<b-up-B\><rsub|\<b-e\>>\<mathd\>x
  </equation>

  \;

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\><glossary-explain|<math|\<b-up-T\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#5230\>\<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#7684\>\<#8F6C\>\<#6362\>\<#77E9\>\<#9635\>>
  <glossary-explain|<math|\<b-up-K\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>>

  <\equation>
    \<b-up-K\><rsub|\<b-l\>>=\<b-up-T\><rsup|T><rsub|\<b-l\>>\<b-up-K\><rsub|\<b-e\>>\<b-up-T\><rsub|\<b-l\>>
  </equation>

  \;

  \<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\><glossary-explain|<math|\<b-up-T\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#5230\>\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7684\>\<#8F6C\>\<#6362\>\<#77E9\>\<#9635\>>
  <glossary-explain|<math|\<b-up-K\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>>

  <\equation>
    \<b-up-K\><rsub|>=\<b-up-T\><rsup|T>\<b-up-K\><rsub|\<b-l\>>\<b-up-T\>
  </equation>

  \;

  <subsection|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>>

  \;

  \<#6746\>\<#4E24\>\<#7AEF\>\<#8282\>\<#70B9\>\<#5750\>\<#6807\>\<#8BB0\>\<#4E3A\>
  <math|P<rsub|1>=<around*|(|x<rsub|1>,y<rsub|1>,z<rsub|1>|)>,P<rsub|2>=<around*|(|x<rsub|2>,y<rsub|2>,z<rsub|2>|)>><glossary-explain|<math|\<b-i\>>|\<#6574\>\<#4F53\><math|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-j\>>|\<#6574\>\<#4F53\><math|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-k\>>|\<#6574\>\<#4F53\><math|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-i\><rprime|'>>|\<#5C40\>\<#90E8\><math|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-j\><rprime|'>>|\<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>>
  <glossary-explain|<math|\<b-k\><rprime|'>>|\<#5C40\>\<#90E8\><math|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>>

  <subsubsection|\<#4E8C\>\<#7EF4\>>

  \;

  \<#6746\>\<#7684\>\<#8F74\>\<#5411\>\<#5411\>\<#91CF\>\<#8BB0\>\<#4E3A\><glossary-explain|<math|\<b-l\>>|\<#6746\>\<#7684\>\<#8F74\>\<#5411\>\<#5411\>\<#91CF\>\<#FF08\>\<#5C40\>\<#90E8\><math|x>\<#8F74\>\<#65B9\>\<#5411\>\<#FF09\>>
  <math|\<b-l\>=<wide|P<rsub|1>P<rsub|2>|\<vect\>>=<around*|{|x<rsub|2>-x<rsub|1>,y<rsub|2>-y<rsub|1>|}><rsup|T>=<around*|{|\<mathd\>x,\<mathd\>y|}><rsup|T>>

  \<#5C40\>\<#90E8\><math|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>
  <math|\<b-i\><rprime|'>=<around*|{|<frac|\<mathd\>x|L>,<frac|\<mathd\>y|L>|}><rsup|T>=<around*|{|a<rsub|x>,a<rsub|y>|}><rsup|T>>

  \<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>
  <math|\<b-j\><rprime|'>=<around*|{|b<rsub|x>,b<rsub|y>|}><rsup|T>=<around*|{|-a<rsub|y>,a<rsub|x>|}><rsup|T>>

  <subsubsection|\<#4E09\>\<#7EF4\>>

  \;

  \<#6746\>\<#7684\>\<#8F74\>\<#5411\>\<#5411\>\<#91CF\>\<#8BB0\>\<#4E3A\>
  <math|\<b-l\>=<around*|{|\<mathd\>x,\<mathd\>y\<#FF0C\>\<mathd\>z|}><rsup|T>>

  \<#5C40\>\<#90E8\><math|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>
  <math|\<b-i\><rprime|'>=<around*|{|a<rsub|x>,a<rsub|y>,a<rsub|z>|}><rsup|T>>

  \<#7ED9\>\<#5B9A\>\<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>
  <math|\<b-j\><rprime|'>=<around*|{|b<rsub|x>,b<rsub|y>,b<rsub|z>|}><rsup|T>>

  \<#5219\>\<#5C40\>\<#90E8\><math|z>\<#8F74\>\<#5411\>\<#91CF\>\<#53EF\>\<#7531\>\<#53C9\>\<#4E58\>\<#5F97\>\<#5230\>
  <math|\<b-k\><rprime|'>=\<b-i\><rprime|'>\<times\>\<b-j\><rprime|'>=<around*|{|a<rsub|y>*b<rsub|z>-b<rsub|y>*a<rsub|z>,b<rsub|x>*a<rsub|z>-a<rsub|x>*b<rsub|z>,a<rsub|x>*b<rsub|y>-b<rsub|x>*a<rsub|y>|}><rsup|T>>

  \;

  \<#6307\>\<#5B9A\>\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\><math|x<around*|\<nobracket\>||\<nobracket\>>y>\<#5E73\>\<#9762\>\<#4E00\>\<#70B9\>\<#FF08\>\<#4E0D\>\<#5728\>\<#6746\>\<#8F74\>\<#7EBF\>\<#4E0A\>\<#FF09\><math|P=<around*|(|x<rsub|p>,y<rsub|p>,z<rsub|p>|)>>

  \<#5B9A\>\<#4E49\>\<#5411\>\<#91CF\> <math|\<b-m\>=<wide|P<rsub|1>P|\<vect\>>=<around*|{|x<rsub|p>-x<rsub|1>,y<rsub|p>-y<rsub|1>,z<rsub|p>-z<rsub|1>|}><rsup|T>=<around*|{|p<rsub|x>,p<rsub|y>,p<rsub|z>|}><rsup|T>>

  \<#5C40\>\<#90E8\><math|z>\<#8F74\>\<#5411\>\<#91CF\>\<#53EF\>\<#7531\>\<#53C9\>\<#4E58\>\<#5F97\>\<#5230\>

  <\equation>
    \<b-n\>=\<b-i\><rprime|'>\<times\>\<b-m\>=<around*|{|a<rsub|y>*p<rsub|z>-a<rsub|z>*p<rsub|y>,a<rsub|z>*p<rsub|x>-a<rsub|x>*p<rsub|z>,a<rsub|x>*p<rsub|y>-a<rsub|y>*p<rsub|x>|}><rsup|T>
  </equation>

  \;

  \<#5C40\>\<#90E8\><math|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>
  <math|\<b-k\><rprime|'>=<frac|\<b-n\>|<around*|\||\<b-n\>|\|>>>

  \<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#5411\>\<#91CF\>

  <\equation>
    \<b-m\>=\<b-n\>\<times\>\<b-i\><rprime|'>=<around*|{|<tabular*|<tformat|<table|<row|<cell|a<rsub|y><rsup|2>*p<rsub|x>-a<rsub|x>*a<rsub|y>*p<rsub|y>+a<rsub|z><rsup|2>*p<rsub|x>-a<rsub|x>*a<rsub|z>*p<rsub|z>>>|<row|<cell|a<rsub|x><rsup|2>*p<rsub|y>-a<rsub|x>*a<rsub|y>*p<rsub|x>+a<rsub|z><rsup|2>*p<rsub|y>-a<rsub|y>*a<rsub|z>*p<rsub|z>>>|<row|<cell|a<rsub|x><rsup|2>*p<rsub|z>-a<rsub|x>*a<rsub|z>*p<rsub|x>-a<rsub|y>*a<rsub|z>*p<rsub|y>+a<rsub|y><rsup|2>*p<rsub|z>>>>>>|}>
  </equation>

  \;

  \<#5C40\>\<#90E8\><math|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>
  <math|\<b-j\><rprime|'>=<frac|\<b-m\>|<around*|\||\<b-m\>|\|>>>

  \;

  \<#5411\>\<#91CF\><math|\<b-m\>>\<#9ED8\>\<#8BA4\>\<#9009\>\<#4E3A\>\<#6574\>\<#4F53\><math|z>\<#8F74\>\<#FF0C\>\<#5373\><math|\<b-m\>=\<b-k\>>

  <\equation>
    \<b-j\><rprime|'>=<frac|1|<sqrt|1-a<rsub|z><rsup|2>>><around*|{|-a<rsub|x>*a<rsub|z>,-a<rsub|y>*a<rsub|z>,1-a<rsub|z><rsup|2>|}><rsup|T>
  </equation>

  <\equation>
    \<b-k\><rprime|'>=<frac|1|<sqrt|1-a<rsub|z><rsup|2>>><around*|{|a<rsub|y>,-a<rsub|x>,0|}><rsup|T>
  </equation>

  \;

  \<#5F53\><math|\<b-l\>\<parallel\>\<b-k\>>\<#65F6\>\<#FF0C\>\<#53D6\>\<#4E3A\>\<#6574\>\<#4F53\><math|y>\<#8F74\>\<#FF0C\>\<#5373\><math|\<b-m\>=\<b-j\>>

  <\equation>
    \<b-j\><rprime|'>=<frac|1|<sqrt|1-a<rsub|y><rsup|2>>><around*|{|-a<rsub|x>*a<rsub|y>,1-a<rsub|y><rsup|2>,-a<rsub|y>*a<rsub|z>|}><rsup|T>=<around*|{|0,1,0|}><rsup|T>
  </equation>

  <\equation>
    \<b-k\><rprime|'>=<around*|{|-1,0,0|}><rsup|T>
  </equation>

  \;

  \<#6216\>\<#6307\>\<#5B9A\>\<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#4E0E\>\<#6574\>\<#4F53\><math|y>\<#8F74\>\<#5728\>\<#5C40\>\<#90E8\><math|y<rsub|>z>\<#5E73\>\<#9762\>\<#6295\>\<#5F71\>\<#7684\>\<#5939\>\<#89D2\>\<#4E3A\><math|\<gamma\>>\<#FF08\>\<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#5728\>\<#9006\>\<#65F6\>\<#9488\>\<#65B9\>\<#5411\>\<#FF09\>

  \<#6574\>\<#4F53\><math|y>\<#8F74\>\<#5728\>\<#5C40\>\<#90E8\><math|x>\<#8F74\>\<#4E0A\>\<#7684\>\<#6295\>\<#5F71\>\<#5411\>\<#91CF\>\<#53EF\>\<#8868\>\<#793A\>\<#4E3A\><math|<around*|(|\<b-j\>\<cdot\>\<b-i\><rprime|'>|)>\<b-i\><rprime|'>>\<#FF0C\>\<#5219\>\<#6574\>\<#4F53\><math|y>\<#8F74\>\<#5728\>\<#5C40\>\<#90E8\><math|y<rsub|>z>\<#5E73\>\<#9762\>\<#6295\>\<#5F71\>\<#5411\>\<#91CF\>\<#4E3A\>

  <\equation>
    \<b-p\>=\<b-j\>-<around*|(|\<b-j\>\<cdot\>\<b-i\><rprime|'>|)>\<b-i\><rprime|'>=<around*|{|-a<rsub|x>*a<rsub|y>,1-a<rsub|y><rsup|2>,-a<rsub|y>*a<rsub|z>|}><rsup|T>
  </equation>

  \;

  \<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>

  <\equation>
    \<b-j\><rprime|'>=<frac|1|<around*|\||\<b-p\>|\|>>\<b-up-R\>\<b-p\>
  </equation>

  \;

  \<#5176\>\<#4E2D\>\<#FF0C\>\<#8F6C\>\<#52A8\>\<#77E9\>\<#9635\>

  <\equation>
    \<b-up-R\>=<with|math-level|1|<around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|3|3|cell-rborder|0ln>|<table|<row|<cell|cos
    \<gamma\>+a<rsub|x><rsup|2>*<around|(|1-cos
    \<gamma\>|)>>|<cell|a<rsub|x>*a<rsub|y>*<around|(|1-cos
    \<gamma\>|)>-a<rsub|z>*sin \<gamma\>>|<cell|a<rsub|x>*a<rsub|z>*<around|(|1-cos
    \<gamma\>|)>+a<rsub|y>*sin \<gamma\>>>|<row|<cell|a<rsub|y>*a<rsub|x>*<around|(|1-cos
    \<gamma\>|)>+a<rsub|z>*sin \<gamma\>>|<cell|cos
    \<gamma\>+a<rsub|y><rsup|2>*<around|(|1-cos
    \<gamma\>|)>>|<cell|a<rsub|y>*a<rsub|z>*<around|(|1-cos
    \<gamma\>|)>-a<rsub|x>*sin \<gamma\>>>|<row|<cell|a<rsub|z>*a<rsub|x>*<around|(|1-cos
    \<gamma\>|)>-a<rsub|y>*sin \<gamma\>>|<cell|a<rsub|z>*a<rsub|y>*<around|(|1-cos
    \<gamma\>|)>+a<rsub|x>*sin \<gamma\>>|<cell|cos
    \<gamma\>+a<rsub|z><rsup|2>*<around|(|1-cos \<gamma\>|)>>>>>>|]>>
  </equation>

  \;

  \<#6216\>\<#4F7F\>\<#7528\>\<#7F57\>\<#5FB7\>\<#91CC\>\<#683C\>\<#65AF\>\<#65CB\>\<#8F6C\>\<#516C\>\<#5F0F\>

  <\equation>
    \<b-up-R\><with|math-font-series|bold|\<b-p\>>=<with|math-font-series|bold|\<b-p\>>*cos
    \<gamma\>+\<b-i\><rprime|'>\<times\><with|math-font-series|bold|\<b-p\>*>sin
    \<gamma\>+<around|(|\<b-i\><rprime|'>\<cdot\>\<b-p\>|)>*\<b-i\><rprime|'>*<around|(|1-cos
    \<gamma\>|)>
  </equation>

  \;

  \<#5C40\>\<#90E8\><math|y>\<#8F74\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>

  <\equation>
    \<b-j\><rprime|'>=<frac|1|<sqrt|1-a<rsub|y><rsup|2>>><around*|{|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<table|<row|<cell|-a<rsub|x>*a<rsub|y>cos
    \<gamma\>-a<rsub|z>*sin \<gamma\>>>|<row|<cell|<around*|(|1-a<rsub|y><rsup|2>|)>cos
    \<gamma\>>>|<row|<cell|a<rsub|x>*sin \<gamma\>-a<rsub|y>*a<rsub|z>*cos
    \<gamma\>>>>>>|}>
  </equation>

  \;

  \;

  \<#5C40\>\<#90E8\><math|z>\<#8F74\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>

  <\equation>
    \<b-k\><rprime|'>=\<b-i\><rprime|'>\<times\>\<b-j\><rprime|'>=<frac|1|<sqrt|1-a<rsub|y><rsup|2>>><around*|{|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<table|<row|<cell|a<rsub|z>cos
    \<gamma\>-a<rsub|x>*a<rsub|y>*sin \<gamma\>>>|<row|<cell|-<around*|(|1-a<rsub|y><rsup|2>|)>sin
    \<gamma\>>>|<row|<cell|a<rsub|y>*a<rsub|z>**sin \<gamma\>+a<rsub|x>cos
    \<gamma\>>>>>>|}>
  </equation>

  <subsection|\<#51E0\>\<#4F55\>\<#53D8\>\<#6362\>>

  <subsubsection|\<#5750\>\<#6807\>\<#53D8\>\<#6362\>>

  \;

  \<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#7684\>\<#6807\>\<#51C6\>\<#57FA\><math|\<frak-B\>=<around*|{|\<b-i\>,\<b-j\>,\<b-k\>|}>>

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#7684\>\<#6807\>\<#51C6\>\<#57FA\><math|\<frak-B\><rprime|'>=<around*|{|\<b-i\><rprime|'>,\<b-j\><rprime|'>,\<b-k\><rprime|'>|}>>

  \<#5728\>\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#5185\>\<#8868\>\<#793A\>\<#4E0A\>\<#8FF0\>\<#5411\>\<#91CF\>

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<b-i\>>|<cell|=>|<cell|<around*|{|1,0,0|}><rsup|T><eq-number>>>|<row|<cell|\<b-j\>>|<cell|=>|<cell|<around*|{|0,1,0|}><rsup|T><eq-number>>>|<row|<cell|\<b-k\>>|<cell|=>|<cell|<around*|{|0,0,1|}><rsup|T><eq-number>>>|<row|<cell|\<b-i\><rprime|'>>|<cell|=>|<cell|<around*|{|a<rsub|x>,a<rsub|y>,a<rsub|z>|}><rsup|T><eq-number>>>|<row|<cell|\<b-j\><rprime|'>>|<cell|=>|<cell|<around*|{|b<rsub|x>,b<rsub|y>,b<rsub|z>|}><rsup|T><eq-number><around*|\<nobracket\>|<around*|\<nobracket\>||\<nobracket\>>|\<nobracket\>>>>|<row|<cell|\<b-k\><rprime|'>>|<cell|=>|<cell|<around*|{|c<rsub|x>,c<rsub|y>,c<rsub|z>|}><rsup|T><eq-number>>>>>
  </eqnarray*>

  \;

  \<#8FC7\>\<#6E21\>\<#77E9\>\<#9635\>

  <\equation>
    <label|basetrans><around*|{|\<b-i\>,\<b-j\>,\<b-k\>|}>=<around*|{|\<b-i\><rprime|'>,\<b-j\><rprime|'>,\<b-k\><rprime|'>|}>\<b-up-T\><rsup|>
  </equation>

  <\equation>
    \<b-up-T\>=<around*|[|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|3|3|3|3|cell-valign|c>|<table|<row|<cell|\<b-i\><rprime|'>\<cdot\>\<b-i\>>|<cell|\<b-i\><rprime|'>\<cdot\>\<b-j\>>|<cell|\<b-i\><rprime|'>\<cdot\>\<b-k\>>>|<row|<cell|\<b-j\><rprime|'>\<cdot\>\<b-i\>>|<cell|\<b-j\><rprime|'>\<cdot\>\<b-j\>>|<cell|\<b-j\><rprime|'>\<cdot\>\<b-k\>>>|<row|<cell|\<b-k\><rprime|'>\<cdot\>\<b-i\>>|<cell|\<b-k\><rprime|'>\<cdot\>\<b-j\>>|<cell|\<b-k\><rprime|'>\<cdot\>\<b-k\>>>>>>|]>=<around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|3|3|cell-rborder|0ln>|<table|<row|<cell|a<rsub|x>>|<cell|a<rsub|y>>|<cell|a<rsub|z>>>|<row|<cell|b<rsub|x>>|<cell|b<rsub|y>>|<cell|b<rsub|z>>>|<row|<cell|c<rsub|x>>|<cell|c<rsub|y>>|<cell|c<rsub|z>>>>>>|]>
  </equation>

  \;

  \;

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#7684\>\<#4F4D\>\<#79FB\>\<#5411\>\<#91CF\><math|\<b-d\><rsub|>=<around*|{|\<b-i\><rprime|'>,\<b-j\><rprime|'>,\<b-k\><rprime|'>|}><around*|{|u<rsub|l>,v<rsub|l>,w<rsub|l>|}><rsup|T>>

  \<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#7684\>\<#4F4D\>\<#79FB\>\<#5411\>\<#91CF\><math|\<b-d\>=<around*|{|\<b-i\>,\<b-j\>,\<b-k\>|}><around*|{|u,v,w|}><rsup|T>>

  <\equation>
    <around*|{|\<b-i\><rprime|'>,\<b-j\><rprime|'>,\<b-k\><rprime|'>|}><around*|{|u<rsub|l>,v<rsub|l>,w<rsub|l>|}><rsup|T>=<around*|{|\<b-i\>,\<b-j\>,\<b-k\>|}><around*|{|u,v,w|}><rsup|T>
  </equation>

  \;

  \<#5C06\>\<#5F0F\><eqref|basetrans>\<#4EE3\>\<#5165\>

  <\equation>
    <around*|{|\<b-i\><rprime|'>,\<b-j\><rprime|'>,\<b-k\><rprime|'>|}><rsup|><around*|{|u<rsub|l>,v<rsub|l>,w<rsub|l>|}><rsup|T>=<around*|{|\<b-i\><rprime|'>,\<b-j\><rprime|'>,\<b-k\><rprime|'>|}>\<b-up-T\><around*|{|u,v,w|}><rsup|T>
  </equation>

  \;

  \<#5373\>

  <\equation>
    <around*|{|u<rsub|l>,v<rsub|l>,w<rsub|l>|}><rsup|T>=\<b-up-T\><around*|{|u,v,w|}><rsup|T>
  </equation>

  \;

  \<#6216\>

  <\equation>
    <around*|{|<tabular|<tformat|<table|<row|<cell|u<rsub|l>>>|<row|<cell|v<rsub|l>>>|<row|<cell|w<rsub|l>>>>>>|}>=\<b-up-T\><around*|{|<tabular|<tformat|<table|<row|<cell|u>>|<row|<cell|v>>|<row|<cell|w>>>>>|}>
  </equation>

  \;

  <subsubsection|\<#521A\>\<#4F53\>\<#8F6C\>\<#52A8\>>

  \;

  \<#8282\>\<#70B9\>\<#521A\>\<#57DF\>\<#957F\>\<#5EA6\><math|\<delta\>L>

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\><math|\<b-u\><rsub|\<b-l\>>=<around*|{|u<rsub|l>,v<rsub|l>,\<theta\>|}><rsup|T>>

  \<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#5355\>\<#5143\>\<#8282\>\<#70B9\>\<#5904\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\><math|\<b-u\><rsub|\<b-n\>>=<around*|{|u<rsub|n>,v<rsub|n>,\<theta\>|}><rsup|T>>

  \<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#5B9E\>\<#9645\>\<#8282\>\<#70B9\>\<#5904\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\><math|\<b-u\>=<around*|{|u,v,\<theta\>|}><rsup|T>>

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#521A\>\<#57DF\>\<#5411\>\<#91CF\><math|\<b-r\><rsub|\<b-l\>>=<around*|{|\<delta\>L
  cos\<theta\>,\<delta\>L sin\<theta\>|}><rsup|T>>

  \<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#521A\>\<#57DF\>\<#5411\>\<#91CF\><math|\<b-r\>=\<b-up-T\><rsup|T><around*|{|\<delta\>L
  cos\<theta\>,\<delta\>L sin\<theta\>|}><rsup|T>>

  <\equation>
    \<b-u\>=\<b-u\><rsub|\<b-n\>>+\<b-r\>
  </equation>

  <big-figure|<with|gr-mode|<tuple|edit|line>|gr-frame|<tuple|scale|1cm|<tuple|0.5gw|0.5gh>>|gr-geometry|<tuple|geometry|1par|0.6par>|gr-grid|<tuple|cartesian|<point|0|0>|0.2>|gr-grid-old|<tuple|cartesian|<point|0|0>|0.2>|gr-edit-grid-aspect|<tuple|<tuple|axes|none>|<tuple|1|none>|<tuple|10|none>>|gr-edit-grid|<tuple|cartesian|<point|0|0>|0.2>|gr-edit-grid-old|<tuple|cartesian|<point|0|0>|0.2>|gr-grid-aspect-props|<tuple|<tuple|axes|#808080>|<tuple|1|#c0c0c0>|<tuple|10|#e0e0ff>>|gr-grid-aspect|<tuple|<tuple|axes|#808080>|<tuple|1|#c0c0c0>|<tuple|10|#e0e0ff>>|<graphics||<point|-6|0>|<point|-5|0>|<point|5|0>|<point|6|0>|<point|-4|0>|<point|-3|0>|<point|-2|0>|<point|-1|0>|<point|0|0>|<point|1|0>|<point|2|0>|<point|3|0>|<point|4|0>|<point|-5|-0.4>|<point|5|-0.4>|<spline|<point|-5|-0.4>|<point|-3.0|-0.8>|<point|0.0|-1.0>|<point|3.0|-0.8>|<point|5.0|-0.4>>|<line|<point|-5|-0.4>|<point|-6.0|0.0>>|<line|<point|5|-0.4>|<point|6.0|0.0>|<point|6.0|0.0>>>>|>

  \;

  \;

  <subsection|\<#4E8C\>\<#7EF4\>\<#6841\>\<#67B6\>\<#5355\>\<#5143\>>

  \;

  \;

  \<#5FAE\>\<#89C2\>\<#5C42\>\<#6B21\>\<#4EC5\>\<#5305\>\<#542B\>\<#4E00\>\<#4E2A\>\<#5E94\>\<#53D8\>\<#5206\>\<#91CF\>\<#FF0C\>\<#5373\>\<#8F74\>\<#5411\>\<#5E94\>\<#53D8\><math|\<varepsilon\>><glossary-explain|<math|\<varepsilon\>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#622A\>\<#9762\>\<#8F74\>\<#5411\>\<#5E94\>\<#53D8\>>\<#FF0C\>\<#5BF9\>\<#5E94\>\<#5B8F\>\<#89C2\>\<#5C42\>\<#6B21\>\<#4E3A\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\><math|u<rsub|e>><glossary-explain|<math|u<rsub|e>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\>>\<#FF0C\>\<#5219\>\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\><math|\<b-varepsilon\>><glossary-explain|<math|\<b-varepsilon\>>|\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\>>\<#4E0E\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>\<b-u\><rsub|\<b-e\>><glossary-explain|<math|\<b-u\><rsub|\<b-e\>>>|\<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>>

  <\equation>
    \<b-varepsilon\>=<around*|{|\<varepsilon\>|}><rsup|T>
  </equation>

  <\equation>
    \<b-u\><rsub|\<b-e\>>=<around*|{|u<rsub|e>|}><rsup|T>
  </equation>

  \;

  \<#6750\>\<#6599\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\><glossary-explain|<math|\<sigma\>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#622A\>\<#9762\>\<#8F74\>\<#5411\>\<#5E94\>\<#529B\>>
  <glossary-explain|<math|D>|\<#6750\>\<#6599\>\<#5207\>\<#7EBF\>\<#6A21\>\<#91CF\>>

  <\equation>
    \<b-up-D\>=<around*|[|D|]>=<around*|[|<frac|\<partial\>\<sigma\>|\<partial\>\<varepsilon\>>|]>
  </equation>

  \;

  \<#622A\>\<#9762\>\<#5C42\>\<#6B21\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\><glossary-explain|<math|A>|\<#622A\>\<#9762\>\<#9762\>\<#79EF\>>\ 

  <\equation>
    \<b-up-D\><rsub|\<b-e\>>=<around*|[|D<rsub|a>|]>=<around*|[|<frac|\<partial\>F|\<partial\>\<varepsilon\>>|]>=<around*|[|<frac|\<partial\>\<sigma\>|\<partial\>\<varepsilon\>>A|]>=<around*|[|D<rsub|>A|]>
  </equation>

  \;

  \<#5355\>\<#5143\>\<#5185\>\<#90E8\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\><math|d<around*|(|x|)>><glossary-explain|<math|d<around*|(|x|)>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#5185\>\<#90E8\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\>>\<#91C7\>\<#7528\>\<#7EBF\>\<#6027\>\<#63D2\>\<#503C\>

  <\equation>
    d<around*|(|x|)>=<dfrac|x|L>u<rsub|e>=\<xi\>u<rsub|e>
  </equation>

  \;

  \<#5C0F\>\<#53D8\>\<#5F62\>\<#5047\>\<#5B9A\>\<#4E0B\>\<#FF0C\>\<#5E94\>\<#53D8\>\<#4E0E\>\<#53D8\>\<#5F62\>\<#7684\>\<#5173\>\<#7CFB\>

  <\equation>
    \<varepsilon\>=<frac|\<partial\>u|\<partial\>x>=<dfrac|1|L>u<rsub|e>
  </equation>

  \;

  \<#5373\>\<#5E94\>\<#53D8\>-\<#53D8\>\<#5F62\>\<#5173\>\<#7CFB\>\<#77E9\>\<#9635\><math|\<b-up-B\><rsub|\<b-e\>>>

  <\equation>
    \<b-up-B\><rsub|\<b-e\>>=<around*|[|<dfrac|1|L>|]>
  </equation>

  \;

  \;

  \<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>

  <\equation>
    \<b-up-K\><rsub|\<b-e\>>=<big|int><rsup|L><rsub|0>\<b-up-B\><rsup|T><rsub|\<b-e\>>\<b-up-D\><rsub|\<b-e\>>\<b-up-B\><rsub|\<b-e\>>\<mathd\>x=<big|int><rsup|L><rsub|0><around*|[|<dfrac|1|L>D*<rsub|a><dfrac|1|L>|]>\<mathd\>x=<around*|[|<frac|1|L><big|int><rsup|1><rsub|0>D<rsub|a>*\<mathd\>\<xi\>|]>=<frac|D<rsub|a>|L><around*|[|1|]>
  </equation>

  \;

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\><math|\<b-u\><rsub|\<b-l\>>><glossary-explain|<math|\<b-u\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>>

  <\equation>
    \<b-u\><rsub|\<b-l\>>=<around*|{|<tabular|<tformat|<table|<row|<cell|u<rsub|l1>>|<cell|v<rsub|l1>>|<cell|u<rsub|l2>>|<cell|v<rsub|l2>>>>>>|}><rsup|T>
  </equation>

  \;

  <math|\<b-u\><rsub|\<b-l\>>>\<#4E0E\><math|\<b-u\><rsub|\<b-e\>>>\<#7684\>\<#5173\>\<#7CFB\>

  <\equation>
    u<rsub|e>=-u<rsub|l1>+u<rsub|l2>
  </equation>

  \;

  \<#5373\>

  <\equation>
    \<b-u\><rsub|\<b-e\>>=<around*|[|<tabular|<tformat|<table|<row|<cell|-1>|<cell|0>|<cell|1>|<cell|0>>>>>|]>\<b-u\><rsub|\<b-l\>>=\<b-up-T\><rsub|\<b-l\>>\<b-u\><rsub|\<b-l\>>
  </equation>

  <\equation>
    \<b-up-T\><rsub|\<b-l\>>=<around*|[|<tabular|<tformat|<table|<row|<cell|-1>|<cell|0>|<cell|1>|<cell|0>>>>>|]>
  </equation>

  \;

  \<#5C40\>\<#90E8\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>

  <\equation>
    \<b-up-K\><rsub|\<b-l\>>=<frac|D<rsub|a>|L><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|1>|<cell|0>|<cell|-1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|-1>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>>>>>|]>
  </equation>

  \;

  \<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\><math|\<b-u\>><glossary-explain|<math|\<b-u\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>>

  <\equation>
    \<b-u\>=<around*|{|<tabular|<tformat|<table|<row|<cell|u<rsub|1>>|<cell|v<rsub|1>>|<cell|u<rsub|2>>|<cell|v<rsub|2>>>>>>|}><rsup|T>
  </equation>

  \;

  <math|\<b-u\>>\<#4E0E\><math|\<b-u\><rsub|\<b-l\>>>\<#7684\>\<#5173\>\<#7CFB\>

  <\equation>
    \<b-u\><rsub|\<b-l\>>=\<b-up-T\><rsub|>\<b-u\>
  </equation>

  <\equation>
    \<b-up-T\>=<around*|[|<tabular|<tformat|<table|<row|<cell|a<rsub|x>>|<cell|a<rsub|y>>|<cell|0>|<cell|0>>|<row|<cell|-a<rsub|y>>|<cell|a<rsub|x>>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|a<rsub|x>>|<cell|a<rsub|y>>>|<row|<cell|0>|<cell|0>|<cell|-a<rsub|y>>|<cell|a<rsub|x>>>>>>|]>
  </equation>

  \;

  \<#6574\>\<#4F53\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>

  <\equation>
    \<b-up-K\>=<frac|D<rsub|a>|L><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|a<rsub|x><rsup|2>>|<cell|a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|x><rsup|2>>|<cell|-a<rsub|x>*a<rsub|y>>>|<row|<cell|a<rsub|x>*a<rsub|y>>|<cell|a<rsub|y><rsup|2>>|<cell|-a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|y><rsup|2>>>|<row|<cell|-a<rsub|x><rsup|2>>|<cell|-a<rsub|x>*a<rsub|y>>|<cell|a<rsub|x><rsup|2>>|<cell|a<rsub|x>*a<rsub|y>>>|<row|<cell|-a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|y><rsup|2>>|<cell|a<rsub|x>*a<rsub|y>>|<cell|a<rsub|y><rsup|2>>>>>>|]>
  </equation>

  \;

  \<#8D28\>\<#91CF\>\<#77E9\>\<#9635\>\<#7684\>\<#63A8\>\<#5BFC\>\<#9700\>\<#8981\>\<#8003\>\<#8651\>\<#6A2A\>\<#5411\>\<#53D8\>\<#5F62\><math|v>

  <\equation>
    \<b-up-N\><rsub|\<b-l\>>=<around*|[|<tabular*|<tformat|<table|<row|<cell|1-<frac|x|L>>|<cell|0>|<cell|<frac|x|L>>|<cell|0>>|<row|<cell|0>|<cell|1-<frac|x|L>>|<cell|0>|<cell|<frac|x|L>>>>>>|]>=<around*|[|<tabular*|<tformat|<table|<row|<cell|1-\<xi\>>|<cell|0>|<cell|\<xi\>>|<cell|0>>|<row|<cell|0>|<cell|1-\<xi\>>|<cell|0>|<cell|\<xi\>>>>>>|]>
  </equation>

  <\equation>
    <tabular*|<tformat|<cwith|1|-1|2|2|cell-halign|l>|<table|<row|<cell|\<b-up-M\><rsub|\<b-l\>>=<big|int><rsub|0><rsup|L><with|math-font-family|bf|\<b-up-N\><rsub|\<b-l\>><rsup|T><with|math-font-series|bold|\<rho\>>\<b-up-N\><rsub|\<b-l\>>>A<math-up|d>x>|<cell|=\<rho\>A<rsub|>L<big|int><rsub|0><rsup|L><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|<around|(|1-\<xi\>|)><rsup|2>>|<cell|0>|<cell|<around|(|1-\<xi\>|)>*\<xi\>>|<cell|0>>|<row|<cell|0>|<cell|<around|(|1-\<xi\>|)><rsup|2>>|<cell|0>|<cell|<around|(|1-\<xi\>|)>*\<xi\>>>|<row|<cell|<around|(|1-\<xi\>|)>*\<xi\>>|<cell|0>|<cell|\<xi\><rsup|2>>|<cell|0>>|<row|<cell|0>|<cell|<around|(|1-\<xi\>|)>*\<xi\>>|<cell|0>|<cell|\<xi\><rsup|2>>>>>>|]><math-up|d>x>|<cell|>>|<row|<cell|>|<cell|=<frac|\<rho\>A<rsub|>L|6><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|2>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|2>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|2>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|2>>>>>|]>>|<cell|>>>>>
  </equation>

  <\equation>
    \<b-up-M\>=<frac|\<rho\>A<rsub|>L|6><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|2*<around*|(|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>|)>>|<cell|0>|<cell|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>>|<cell|0>>|<row|<cell|0>|<cell|2*<around*|(|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>|)>>|<cell|0>|<cell|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>>>|<row|<cell|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>>|<cell|0>|<cell|2*<around*|(|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>|)>>|<cell|0>>|<row|<cell|0>|<cell|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>>|<cell|0>|<cell|2*<around*|(|a<rsub|x><rsup|2>+a<rsub|y><rsup|2>|)>>>>>>|]>=<frac|\<rho\>A<rsub|>L|6><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|2>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|2>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|2>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|2>>>>>|]>
  </equation>

  \;

  \<#96C6\>\<#4E2D\>\<#8D28\>\<#91CF\>\<#77E9\>\<#9635\>

  <\equation>
    \<b-up-M\>=<frac|\<rho\>A<rsub|>L|2><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|4|4|cell-rborder|0ln>|<table|<row|<cell|1>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|1>>>>>|]>
  </equation>

  \;

  <subsection|\<#4E09\>\<#7EF4\>\<#6841\>\<#67B6\>\<#5355\>\<#5143\>>

  \;

  <\equation>
    \<b-u\><rsub|\<b-l\>>=<around*|{|<tabular|<tformat|<table|<row|<cell|u<rsub|l1>>|<cell|v<rsub|l1>>|<cell|w<rsub|l1>>|<cell|u<rsub|l2>>|<cell|v<rsub|l2>>|<cell|w<rsub|l2>>>>>>|}><rsup|T>
  </equation>

  <\equation>
    \<b-up-T\><rsub|\<b-l\>>=<around*|[|<tabular|<tformat|<table|<row|<cell|-1>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>>>>|]>
  </equation>

  <\equation>
    \<b-up-K\><rsub|\<b-l\>>=<frac|D<rsub|a>|L><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<cwith|1|-1|6|6|cell-halign|c>|<cwith|1|-1|6|6|cell-rborder|0ln>|<table|<row|<cell|1>|<cell|0>|<cell|0>|<cell|-1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|-1>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>>>>|]>
  </equation>

  \;

  <\equation>
    \<b-u\>=<around*|{|<tabular|<tformat|<table|<row|<cell|u<rsub|1>>|<cell|v<rsub|1>>|<cell|w<rsub|1>>|<cell|u<rsub|2>>|<cell|v<rsub|2>>|<cell|w<rsub|2>>>>>>|}><rsup|T>
  </equation>

  <\equation>
    \<b-up-K\>=<frac|D<rsub|a>|L><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<cwith|1|-1|6|6|cell-halign|c>|<cwith|1|-1|6|6|cell-rborder|0ln>|<table|<row|<cell|a<rsub|x><rsup|2>>|<cell|a<rsub|x>*a<rsub|y>>|<cell|a<rsub|x>*a<rsub|z>>|<cell|-a<rsub|x><rsup|2>>|<cell|-a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|x>*a<rsub|z>>>|<row|<cell|a<rsub|x>*a<rsub|y>>|<cell|a<rsub|y><rsup|2>>|<cell|a<rsub|y>*a<rsub|z>>|<cell|-a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|y><rsup|2>>|<cell|-a<rsub|y>*a<rsub|z>>>|<row|<cell|a<rsub|x>*a<rsub|z>>|<cell|a<rsub|y>*a<rsub|z>>|<cell|a<rsub|z><rsup|2>>|<cell|-a<rsub|x>*a<rsub|z>>|<cell|-a<rsub|y>*a<rsub|z>>|<cell|-a<rsub|z><rsup|2>>>|<row|<cell|-a<rsub|x><rsup|2>>|<cell|-a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|x>*a<rsub|z>>|<cell|a<rsub|x><rsup|2>>|<cell|a<rsub|x>*a<rsub|y>>|<cell|a<rsub|x>*a<rsub|z>>>|<row|<cell|-a<rsub|x>*a<rsub|y>>|<cell|-a<rsub|y><rsup|2>>|<cell|-a<rsub|y>*a<rsub|z>>|<cell|a<rsub|x>*a<rsub|y>>|<cell|a<rsub|y><rsup|2>>|<cell|a<rsub|y>*a<rsub|z>>>|<row|<cell|-a<rsub|x>*a<rsub|z>>|<cell|-a<rsub|y>*a<rsub|z>>|<cell|-a<rsub|z><rsup|2>>|<cell|a<rsub|x>*a<rsub|z>>|<cell|a<rsub|y>*a<rsub|z>>|<cell|a<rsub|z><rsup|2>>>>>>|]>
  </equation>

  \;

  \<#4E00\>\<#81F4\>\<#8D28\>\<#91CF\>\<#77E9\>\<#9635\>

  \;

  <\equation>
    \<b-up-M\>=<frac|\<rho\>A<rsub|>L|6><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<cwith|1|-1|6|6|cell-halign|c>|<cwith|1|-1|6|6|cell-rborder|0ln>|<table|<row|<cell|2>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|2>|<cell|0>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|2>|<cell|0>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|0>|<cell|2>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|2>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|2>>>>>|]>
  </equation>

  \;

  \<#96C6\>\<#4E2D\>\<#8D28\>\<#91CF\>\<#77E9\>\<#9635\>

  <\equation>
    \<b-up-M\>=<frac|\<rho\>A<rsub|>L|2><around*|[|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|c>|<cwith|1|-1|3|3|cell-halign|c>|<cwith|1|-1|4|4|cell-halign|c>|<cwith|1|-1|5|5|cell-halign|c>|<cwith|1|-1|6|6|cell-halign|c>|<cwith|1|-1|6|6|cell-rborder|0ln>|<table|<row|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|1>>>>>|]>
  </equation>
</body>

<\initial>
  <\collection>
    <associate|bg-color|pastel green>
    <associate|info-flag|minimal>
    <associate|magnification|1>
    <associate|page-medium|paper>
    <associate|page-screen-margin|false>
    <associate|page-width-margin|false>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|\<#57FA\>\<#53D8\>\<#6362\>|<tuple|3|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|1.8|2>>
    <associate|auto-11|<tuple|1.8|2>>
    <associate|auto-12|<tuple|1.8|2>>
    <associate|auto-13|<tuple|1.8|2>>
    <associate|auto-14|<tuple|1.8|2>>
    <associate|auto-15|<tuple|1.8|2>>
    <associate|auto-16|<tuple|1.9|2>>
    <associate|auto-17|<tuple|1.9|2>>
    <associate|auto-18|<tuple|1.10|2>>
    <associate|auto-19|<tuple|1.10|2>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-20|<tuple|1.1|2>>
    <associate|auto-21|<tuple|1.1|2>>
    <associate|auto-22|<tuple|1.1|2>>
    <associate|auto-23|<tuple|1.1|2>>
    <associate|auto-24|<tuple|1.1|2>>
    <associate|auto-25|<tuple|1.1|2>>
    <associate|auto-26|<tuple|1.1|2>>
    <associate|auto-27|<tuple|1.1.1|2>>
    <associate|auto-28|<tuple|1.1.1|2>>
    <associate|auto-29|<tuple|1.1.2|2>>
    <associate|auto-3|<tuple|1|1>>
    <associate|auto-30|<tuple|1.2|4>>
    <associate|auto-31|<tuple|1.2.1|4>>
    <associate|auto-32|<tuple|1.2.2|4>>
    <associate|auto-33|<tuple|1.1|5>>
    <associate|auto-34|<tuple|1.3|5>>
    <associate|auto-35|<tuple|1.3|5>>
    <associate|auto-36|<tuple|1.3|5>>
    <associate|auto-37|<tuple|1.3|5>>
    <associate|auto-38|<tuple|1.3|5>>
    <associate|auto-39|<tuple|1.38|5>>
    <associate|auto-4|<tuple|1|1>>
    <associate|auto-40|<tuple|1.38|5>>
    <associate|auto-41|<tuple|1.39|5>>
    <associate|auto-42|<tuple|1.40|6>>
    <associate|auto-43|<tuple|1.44|6>>
    <associate|auto-44|<tuple|1.49|6>>
    <associate|auto-45|<tuple|1.4|7>>
    <associate|auto-5|<tuple|1|1>>
    <associate|auto-6|<tuple|1|1>>
    <associate|auto-7|<tuple|1|1>>
    <associate|auto-8|<tuple|1|1>>
    <associate|auto-9|<tuple|1|1>>
    <associate|basetrans|<tuple|1.30|4>>
    <associate|bt|<tuple|3|?>>
    <associate|d2u|<tuple|1.2|?>>
    <associate|e2u|<tuple|1.3|?>>
    <associate|magicparlabel-686|<tuple|1.45|?>>
    <associate|minp|<tuple|1.7|?>>
    <associate|pe|<tuple|1.1|1>>
    <associate|peu|<tuple|1.6|2>>
    <associate|u2d|<tuple|1.3|1>>
    <associate|u2e|<tuple|1.4|2>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal||<pageref|auto-33>>
    </associate>
    <\associate|gly>
      <tuple|normal|<with|mode|<quote|math>|\<Pi\><rsub|p>>|\<#7CFB\>\<#7EDF\>\<#52BF\>\<#80FD\>|<pageref|auto-3>>

      <tuple|normal|<with|mode|<quote|math>|\<b-sigma\>>|\<#5E94\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-4>>

      <tuple|normal|<with|mode|<quote|math>|\<b-varepsilon\>>|\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\>|<pageref|auto-5>>

      <tuple|normal|<with|mode|<quote|math>|\<b-d\>>|\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-6>>

      <tuple|normal|<with|mode|<quote|math>|\<b-F\><rsub|\<b-b\>>>|\<#4F53\>\<#79EF\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-7>>

      <tuple|normal|<with|mode|<quote|math>|\<b-F\><rsub|\<b-s\>>>|\<#4F53\>\<#79EF\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-8>>

      <tuple|normal|<with|mode|<quote|math>|\<b-P\>>|\<#8282\>\<#70B9\>\<#529B\>\<#5411\>\<#91CF\>|<pageref|auto-9>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-K\><rsub|\<b-e\>>>|\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>\<#FF08\>\<#5BF9\>\<#5E94\>\<#76F8\>\<#5BF9\>\<#53D8\>\<#5F62\>\<#FF09\>|<pageref|auto-10>>

      <tuple|normal|<with|mode|<quote|math>|V>|\<#5355\>\<#5143\>\<#4F53\>\<#79EF\>|<pageref|auto-11>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-B\><rsub|\<b-e\>>>|\<#5E94\>\<#53D8\>-\<#53D8\>\<#5F62\>\<#5173\>\<#7CFB\>\<#77E9\>\<#9635\>|<pageref|auto-12>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-D\>>|\<#6750\>\<#6599\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\>|<pageref|auto-13>>

      <tuple|normal|<with|mode|<quote|math>|L>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#957F\>\<#5EA6\>|<pageref|auto-14>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-D\><rsub|\<b-e\>>>|\<#622A\>\<#9762\>\<#5C42\>\<#6B21\>\<#672C\>\<#6784\>\<#77E9\>\<#9635\>|<pageref|auto-15>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-T\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#5230\>\<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#7684\>\<#8F6C\>\<#6362\>\<#77E9\>\<#9635\>|<pageref|auto-16>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-K\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>|<pageref|auto-17>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-T\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#5230\>\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7684\>\<#8F6C\>\<#6362\>\<#77E9\>\<#9635\>|<pageref|auto-18>>

      <tuple|normal|<with|mode|<quote|math>|\<b-up-K\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#5355\>\<#5143\>\<#521A\>\<#5EA6\>\<#77E9\>\<#9635\>|<pageref|auto-19>>

      <tuple|normal|<with|mode|<quote|math>|\<b-i\>>|\<#6574\>\<#4F53\><with|mode|<quote|math>|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-21>>

      <tuple|normal|<with|mode|<quote|math>|\<b-j\>>|\<#6574\>\<#4F53\><with|mode|<quote|math>|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-22>>

      <tuple|normal|<with|mode|<quote|math>|\<b-k\>>|\<#6574\>\<#4F53\><with|mode|<quote|math>|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-23>>

      <tuple|normal|<with|mode|<quote|math>|\<b-i\><rprime|'>>|\<#5C40\>\<#90E8\><with|mode|<quote|math>|x>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-24>>

      <tuple|normal|<with|mode|<quote|math>|\<b-j\><rprime|'>>|\<#5C40\>\<#90E8\><with|mode|<quote|math>|y>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-25>>

      <tuple|normal|<with|mode|<quote|math>|\<b-k\><rprime|'>>|\<#5C40\>\<#90E8\><with|mode|<quote|math>|z>\<#8F74\>\<#7684\>\<#5355\>\<#4F4D\>\<#5411\>\<#91CF\>|<pageref|auto-26>>

      <tuple|normal|<with|mode|<quote|math>|\<b-l\>>|\<#6746\>\<#7684\>\<#8F74\>\<#5411\>\<#5411\>\<#91CF\>\<#FF08\>\<#5C40\>\<#90E8\><with|mode|<quote|math>|x>\<#8F74\>\<#65B9\>\<#5411\>\<#FF09\>|<pageref|auto-28>>

      <tuple|normal|<with|mode|<quote|math>|\<varepsilon\>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#622A\>\<#9762\>\<#8F74\>\<#5411\>\<#5E94\>\<#53D8\>|<pageref|auto-35>>

      <tuple|normal|<with|mode|<quote|math>|u<rsub|e>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\>|<pageref|auto-36>>

      <tuple|normal|<with|mode|<quote|math>|\<b-varepsilon\>>|\<#5E94\>\<#53D8\>\<#5411\>\<#91CF\>|<pageref|auto-37>>

      <tuple|normal|<with|mode|<quote|math>|\<b-u\><rsub|\<b-e\>>>|\<#5355\>\<#5143\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-38>>

      <tuple|normal|<with|mode|<quote|math>|\<sigma\>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#622A\>\<#9762\>\<#8F74\>\<#5411\>\<#5E94\>\<#529B\>|<pageref|auto-39>>

      <tuple|normal|<with|mode|<quote|math>|D>|\<#6750\>\<#6599\>\<#5207\>\<#7EBF\>\<#6A21\>\<#91CF\>|<pageref|auto-40>>

      <tuple|normal|<with|mode|<quote|math>|A>|\<#622A\>\<#9762\>\<#9762\>\<#79EF\>|<pageref|auto-41>>

      <tuple|normal|<with|mode|<quote|math>|d<around*|(|x|)>>|\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>\<#5185\>\<#90E8\>\<#8F74\>\<#5411\>\<#53D8\>\<#5F62\>|<pageref|auto-42>>

      <tuple|normal|<with|mode|<quote|math>|\<b-u\><rsub|\<b-l\>>>|\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-43>>

      <tuple|normal|<with|mode|<quote|math>|\<b-u\>>|\<#6574\>\<#4F53\>\<#5750\>\<#6807\>\<#7CFB\>\<#4E0B\>\<#8282\>\<#70B9\>\<#53D8\>\<#5F62\>\<#5411\>\<#91CF\>|<pageref|auto-44>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#672F\>\<#8BED\>\<#8868\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>\<#6746\>\<#7CFB\>\<#5355\>\<#5143\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>\<#5C40\>\<#90E8\>\<#5750\>\<#6807\>\<#7CFB\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20>>

      <with|par-left|<quote|2tab>|1.1.1<space|2spc>\<#4E8C\>\<#7EF4\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-27>>

      <with|par-left|<quote|2tab>|1.1.2<space|2spc>\<#4E09\>\<#7EF4\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>\<#51E0\>\<#4F55\>\<#53D8\>\<#6362\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-30>>

      <with|par-left|<quote|2tab>|1.2.1<space|2spc>\<#5750\>\<#6807\>\<#53D8\>\<#6362\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-31>>

      <with|par-left|<quote|2tab>|1.2.2<space|2spc>\<#521A\>\<#4F53\>\<#8F6C\>\<#52A8\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>\<#4E8C\>\<#7EF4\>\<#6841\>\<#67B6\>\<#5355\>\<#5143\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34>>

      <with|par-left|<quote|1tab>|1.4<space|2spc>\<#4E09\>\<#7EF4\>\<#6841\>\<#67B6\>\<#5355\>\<#5143\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>>
    </associate>
  </collection>
</auxiliary>