module Sec

    importall Mat

    abstract Section
    abstract ElasticSection <: Section

    export Section,
            GeneralSection,
            ElasticSection,ElasticTrussSection,
            setStrain,getForceStiff,updateHistoryPara

    type Profile

        kind::String #// 截面类型
        dparas::Vector{Float64} #// 截面尺寸参数
        pparas::Vector{Float64} #// 截面特性参数

        function Profile(kind::String, dparas::Vector{Float64}, pparas::Vector{Float64})
            if kind == "rectangle"
                b,h = dparas
                A = b*h
            else
                #default
            end
        end
    end

    type GeneralSection <: Section

        # ---桁架截面--- #
        tag::Int64 #// 标签
        A::Float64 #// 截面面积
        I33::Float64 #// 截面惯性矩
        I22::Float64 #// 截面惯性矩
        mat::GeneralMaterial #// 截面材料

        GeneralSection(tag::Int64,A::Float64,mat::Material) = new(tag,A,0.0,0.0,deepcopy(mat))
        GeneralSection(tag::Int64,A::Float64,I33::Float64,I22::Float64,mat::Material) = new(tag,A,I33,I22,deepcopy(mat))
        
    end

    type ElasticTrussSection <: ElasticSection

        # ---桁架截面--- #
        #  
        tag::Int64 #// 标签
        A::Float64 #// 截面面积
        mat::Material #// 截面材料
        Da::Float64 #// 截面轴向刚度
        rho::Float64 #// 线密度
        strain::Float64 #// 截面轴向应变
        stress::Float64 #// 截面轴向应力
        force::Float64 #// 截面轴力

        ElasticTrussSection(tag::Int64,A::Float64,mat::Material) = new(tag,A,deepcopy(mat),mat.tangent*A,mat.rho*A,0.0,0.0,0.0)
        ElasticTrussSection(sec::GeneralSection) = new(sec.tag,sec.A,deepcopy(sec.mat),sec.mat.E0*sec.A,sec.mat.rho*sec.A,0.0,0.0,0.0)
        
    end

    function setStrain(sec::ElasticTrussSection,strain::Float64)
        sec.strain = strain
        nothing
    end

    function getForceStiff(sec::ElasticTrussSection)
        
        setStrain(sec.mat,sec.strain) # 设置材料应变
        getStressTangent(sec.mat) # 计算材料应力及切线刚度
        sec.stress = sec.mat.stress # 由材料状态计算应力
        sec.force = sec.stress*sec.A # 由材料状态计算内力
        sec.Da = sec.mat.tangent*sec.A # 由材料状态计算内力
        nothing
    end

    function getForce(sec::ElasticTrussSection)
        
        setStrain(sec.mat,sec.strain) # 设置材料应变
        getStressTangent(sec.mat) # 计算材料应力及切线刚度
        sec.stress = sec.mat.stress # 由材料状态计算应力
        sec.force = sec.stress*sec.A # 由材料状态计算内力
        nothing

    end

    function updateHistoryPara(sec::ElasticTrussSection)
        updateHistoryPara(sec.mat)
        nothing
    end

end